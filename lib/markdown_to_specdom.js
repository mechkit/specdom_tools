var f = require('functions');
var markdown_specs = require('markdown_specs');

var convertion_functions_default = {
  'md': markdown_specs,
};

function markdown_to_specdom(markdown_strings, convertion_function){
  convertion_function = convertion_function || convertion_functions_default.md;
  var pages = {};
  for ( let page_id in markdown_strings ){
    let page_spec = convertion_function(markdown_strings[page_id]);
    let page_name = page_id.split('/').slice(-1)[0];
    var title = f.pretty_name(page_name);
    page_spec.props = page_spec.props || {};
    page_spec.props['id'] = 'page';
    page_spec.meta = page_spec.meta || {};
    page_spec.meta['page_id'] = page_id;
    page_spec.meta['title'] = title;
    page_spec.meta['page_name'] = page_name;
    pages[page_id] = page_spec;
  }
  return pages;
}

module.exports = markdown_to_specdom;
