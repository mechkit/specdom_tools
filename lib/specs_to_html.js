var mk_HTML = require('./mk_HTML');

/**
 * .
 * @exports
 */
module.exports = function(specs, settings){
  let title = settings.title || '';
  let format = settings.format || 'full';
  let css_files = settings.css_files || [];
  let js_files_pre = settings.js_files_pre || [];
  let js_files_post = settings.js_files_post || [];
  let final_lines = settings.final_lines || [];

  var html_array = [];

  if ( format !== 'body only' ){
    html_array = html_array.concat([
      '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" >',
      '<html>',
      '<head>',
      '<meta charset="UTF-8">',
      '<meta name="viewport" content="width=device-width,initial-scale=1">',
      `<title>${title}</title>`,
    ]);
    css_files.forEach( css_url => {
      html_array.push( `<link rel="stylesheet" href="${css_url}">` );
    });
    js_files_pre.forEach( js_url => {
      html_array.push( `<script type="text/javascript" src="${js_url}"></script>` );
    });
    html_array = html_array.concat([
      '</head>',
      '<body>',
    ]);
  }

  if ( specs.constuctor === Array ) {
    specs.forEach(spec => {
      html_array = html_array.concat( mk_HTML(specs) );
    });
  } else {
    html_array = html_array.concat( mk_HTML(specs) );
  }

  if ( format !== 'body only' ){
    js_files_post.forEach( js_url => {
      html_array.push( `<script type="text/javascript" src="${js_url}"></script>` );
    });
    final_lines.forEach( final_line => {
      html_array.push( final_line );
    });
    html_array = html_array.concat([
      '</body>',
      '</html>',
    ]);
  }

  format = format || 'compact';
  var html;
  if ( format === 'compact' || format === 'body only' ) {
    html_array = html_array.map(function (line) {
      return line.trim();
    });
    html = html_array.join('');
  } else {
    html = html_array.join('\n');
  }


  return html;
};
