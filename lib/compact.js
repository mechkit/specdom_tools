var cspec_replace = {
  'tag': '_',
  'children': '_c',
  'text': '_t',
  'meta': '_m',
  'props': '_p',
  'style': '_s',
};

module.exports = function compact(spec){
  Object.keys(spec).forEach( key =>{
    if ( cspec_replace[key] ){
      spec[cspec_replace[key]] = spec[key];
      delete spec[key];
    }
  });
  if ( spec._c && spec._c.constructor === Array ){
    let children = spec._c;
    spec._c = [];
    children.forEach( subspec => {
      spec._c.push( compact(subspec) );
    });
  }
  return spec;
};
