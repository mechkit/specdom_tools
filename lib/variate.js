function find_variables(specs,var_list){
  var_list = var_list || [];
  if ( specs.meta && specs.meta.var_type === 'variable' ){
    var_list.push( specs );
  }
  if ( specs.children ){
    specs.children.forEach( child_spec => {
      var_list = find_variables(child_spec,var_list);
    });
  }
  return var_list;
}

function set_value(obj,value,location_array){
  var destination = location_array.length === 1;
  if ( destination ) {
    obj[location_array[0]] = value;
    return obj;
  } else {
    obj[location_array[0]] = obj[location_array[0]] || {};
  }
  obj[location_array[0]] = set_value( obj[location_array[0]], value, location_array.slice(1) );
  return obj;
}

module.exports = function variate(specs,page_vars){
  page_vars = page_vars || {};
  var var_list = find_variables(specs);
  var_list.forEach( var_spec => {
    let var_id = var_spec.meta.var_id;
    let var_value = page_vars[var_id];
    if ( var_spec.meta.var_template && ( var_spec.meta.var_template.constructor = Array ) ){
      var_value = var_spec.meta.var_template.map( substring => {
        if ( page_vars[substring] !== undefined ){
          return page_vars[substring];
        } else {
          return substring;
        }
      }).join('');
    }
    let location_array = var_spec.meta.var_location.split('.');
    var_spec = set_value(var_spec,var_value,location_array);
    console.log(var_spec);

  });
  return specs;
};
