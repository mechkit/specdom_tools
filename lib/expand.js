var cspec_replace = {
  '_': 'tag',
  '_c': 'children',
  '_t': 'text',
  '_m': 'meta',
  '_p': 'props',
  '_s': 'style',
};

module.exports = function expand(spec){
  Object.keys(spec).forEach( key =>{
    if ( cspec_replace[key] ){
      spec[cspec_replace[key]] = spec[key];
      delete spec[key];
    }
  });
  if ( spec.children && spec.children.constructor === Array ){
    let children = spec.children;
    spec.children = [];
    children.forEach( subspec => {
      spec.children.push( expand(subspec) );
    });
  }
  return spec;
};
