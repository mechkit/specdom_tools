var default_config = {
  path: './#/',
  root_path: './#/',
  rules: [
    {
      // relative link
      tag: 'a',
      link_type: 'href',
      not: /^https*:\/\/|^\//,
      replace: /^\.\/|\.md/g,
      prepend_path: true,
    },
    {
      // absolute link
      tag: 'a',
      link_type: 'href',
      find: /^\//,
      not: /^\/#\//,
      replace: /\.md/g,
    },
    {
      tag: 'img',
      link_type: 'src',
      prepend_string: '/assets/',
    },
  ]
};
function fix_link_path(specs,config){
  config = Object.assign({}, default_config, config);
  let path = config.path;
  config.rules.forEach( rule => {
    let link_string = specs.props && specs.props[rule.link_type];
    if ( link_string && specs.tag === rule.tag ){
      let should_prepend = true;
      if ( rule.find !== undefined ) {
        should_prepend = link_string.match( rule.find ) !== null;
      }
      if ( rule.not !== undefined ) {
        should_prepend = should_prepend && link_string.match( rule.not ) === null;
      }
      if ( should_prepend ){
        if ( rule.replace !== undefined ){
          link_string = link_string.replace( rule.replace, '' );
        }
        if ( rule.prepend_string !== undefined ){
          link_string = rule.prepend_string + link_string;
        }
        if ( rule.prepend_path === true ){
          link_string = path + link_string;
        }
        specs.props[rule.link_type] = link_string;
      }
    }
  });
  if ( specs.children ){
    specs.children.forEach( subspec => fix_link_path(subspec,config) );
  }
  return specs;
}
module.exports = fix_link_path;
