/**
* mk_html - Makes DOM Element from a ConfigDOM specs object
* @function
* @param  {object} specs ConfigDOM specs object
* @return {string} HTML
*/
var mk_html = function mk_html(specs, level){
  if( !specs ){
    specs = {};
  }
  if (!level ){
    level = 0;
  }

  ///////////////////////////////
  // Prep specs

  if( specs.constructor === Number ){
    specs = specs.toString();
  }

  if( specs.constructor === String ){
    //sdom = document.createTextNode(specs);
    specs = {
      tag: 'textNode',
      text: specs
    };
  }

  ///////////////////////////
  // make element

  var html_array = [];
  var html_line = ' '.repeat(level*2);

  if( specs.tag === 'elem' ) { // NODE ELEMENT
    html_line = specs.elem.outerhtml;
  // } else if( specs.textNode ) {
  } else if( specs.tag === 'textNode' ) {
    html_line = ' '.repeat(level * 2) + specs.text;
  } else { // CONFIG
    if( specs.tag ){
      html_line += '<'+specs.tag;
    } else {
      console.log('Elem type not found.', specs);
      return false;
    }

    if( specs.props ){
      for( var name in specs.props ){
        html_line += ' ' + name + '="' + specs.props[name] + '"';
      }
    }
    if( specs.style ){
      html_line += ' style="';
      for( var name in specs.style ){
        html_line += name + ':' + specs.style[name] + ';';
      }
      html_line += '"';
    }
    html_line += '>';

    html_array.push(html_line);
    html_line = ' '.repeat(level*2);

    if (specs.text ){
      html_line += specs.text;
      html_array.push(html_line);
      html_line = ' '.repeat(level*2);
    }

    if (specs.children && specs.children.length) {
      for (var i = 0; i < specs.children.length; i++) {
        var html_array_children = mk_html( specs.children[i], level+1 );
        html_array = html_array.concat(html_array_children);
      }
    }

    html_line += '</'+specs.tag+'>';
  }
  html_array.push(html_line);

  // html = html_array.join('\n');

  return html_array;
};


module.exports = mk_html;
