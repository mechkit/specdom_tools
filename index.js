module.exports.specs_to_html = require('./lib/specs_to_html');
module.exports.compact = require('./lib/compact');
module.exports.expand = require('./lib/expand');
module.exports.markdown_to_specdom = require('./lib/markdown_to_specdom');
module.exports.fix_link_path = require('./lib/fix_link_path');
module.exports.markdown_specs = require('markdown_specs');
module.exports.variate = require('./lib/variate');

